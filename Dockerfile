FROM bitnami/node:18.17.0

# psql
RUN apt-get install -y postgresql-client

#dredd
RUN npm install -g dredd --unsafe-perm=true --allow-root

#xvfb
RUN apt-get -y install libgtk2.0-0 libgtk-3-0 libnotify-dev libgconf-2-4 libnss3 libxss1 libasound2 libxtst6 xauth xvfb

#redis-cli
RUN cd /tmp && \
    wget http://download.redis.io/redis-stable.tar.gz && \
    tar xvzf redis-stable.tar.gz && \
    cd redis-stable && make && \
    cp src/redis-cli /usr/local/bin/ && \
    rm -rf /tmp/redis-stable

RUN rm -rf /var/lib/apt/lists/*
